
class Grid

  # Start the grid with 1 cell w*h in size
  def initialize(w,h)
    @cols = [w]
    @rows = [h]
    @vals = [[nil]]
  end

  def split_col(col, requested_w)
    w = @cols[col]
    new_w = w - requested_w
    @cols[col] = requested_w 
    @cols.insert(col, new_w)

    @vals.each do |row|
      row.insert(col, row[col])
    end
  end

  def split_row(row, requested_h)
    h = @rows[row]
    new_h = h - requested_h
    @rows[row] = requested_h
    @rows.insert(row, new_h)

    copy = []
    @vals[row].each { |e| copy << e }

    @vals.insert(row, copy)
  end

  # place a value in the cell at col,row
  def set(col, row, value)
    @vals[row][col] = value
  end

  def width_at(col, row)
    total_w = 0

    walk_across(col, row) do |cell|
      total_w += cell[:width]
    end

    total_w
  end

  def height_at(col, row)
    total_h = 0

    walk_down(col, row) do |cell|
      total_h += cell[:height]
    end

    total_h
  end

  def walk_cells(only_free)
    r = 0
    c = 0
    x = 0
    y = 0
    @vals.each do |row|
      height = @rows[r]
      row.each do |value|
        width  =  @cols[c]
        cell_info = {:col => c, :row => r, :width => width, :height => height, :x => x, :y => y, :value => value}
        if only_free
          if free_at(c,r)
            yield cell_info
          end
        else
          yield cell_info
        end
        c += 1
        x += width
      end
      r += 1
      y += height
    end
  end

  # visit empty cells in row starting at col,row
  def walk_across(col, row)
    (col...(@cols.size)).each do |c|
      if free_at(c, row)
        hash = {:col => c, :row => row, :width => @cols[c], :height => @rows[row]}
        yield hash 
      else
	break
      end
    end
  end

  # visit empty cells in col starting at col,row
  def walk_down(col, row)
    (row...(@rows.size)).each do |r|
      if free_at(col, r)
        hash = {:col => col, :row => r, :width => @cols[col], :height => @rows[r]}
        yield hash 
      else
	break
      end
    end
  end

  def info_at(col, row)
    "col=#{col} row=#{row} val=#{@vals[row][col]}"
  end

  def free_at(col, row)
      @vals[row][col] == nil
  end

  def draw_string
    walk_cells(false)
  end
end

