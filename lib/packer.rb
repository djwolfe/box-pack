
# Driver class that attempts to pack() an array of items
# into a space w*h

class Packer
  attr_accessor :w, :l

  def initialize(w,h)
    @grid = Grid.new(w,h)	  
  end
  
  # takes and array of items implementing properties w/l and (settable) x/y
  # e.g. an array of Box objects
  def pack(items)
    items.each do |e|
      puts "Packing #{e} ======"
      pack_item(e)
      p @grid
    end
  end

  def pack_item(e)
    
    @grid.walk_cells(true) do |cell|
      col = cell[:col]
      row = cell[:row]
      free_w = @grid.width_at(col, row)
      free_h = @grid.height_at(col, row)

      puts "Does [#{e}] fit at #{cell}?"
      if e.w <= free_w && e.l <= free_h
        puts "#{e} fits at #{cell}"
        e.x = cell[:x]
        e.y = cell[:y]
        @grid.split_col(col, e.w)
        @grid.split_row(row, e.l)
        @grid.set(col, row, true)
        break;
      end
    end
    
  end

end

