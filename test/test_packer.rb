require 'test/unit'
require_relative '../lib/packer.rb'
require_relative '../lib/box.rb'
require_relative '../lib/grid.rb'

class TestPacker < Test::Unit::TestCase

  def test1
    p = Packer.new(100,100)
	
    item1 = Box.new
    item1.w = 20
    item1.l = 10
    
    item2 = Box.new
    item2.w = 15
    item2.l = 5
    
    items = [item1, item2]
    
    p.pack(items)
    
    items.each { |e| puts e.to_s}
  end

end
	