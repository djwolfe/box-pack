require 'test/unit'
require_relative '../lib/grid.rb'

class TestGrid < Test::Unit::TestCase

  def test1
    # w,h
    g = Grid.new(100, 50)

    g.split_col(0, 50)
    g.split_row(0, 25)
    
    g.set(1,1, "Y")
    
    g.split_row(1, 10)
    
    p g
  end

  def test_width_at
    g = Grid.new(20, 20)

    # (col,row)
    w = g.width_at(0, 0)
    assert_equal 20, w

    g.split_col(0, 10)
    g.split_row(0, 10)

    w = g.width_at(0, 0)
    assert_equal 20, w

    g.set(1, 1, "X")
    
    w = g.width_at(0, 0)
    assert_equal 20, w

    w = g.width_at(0, 1)
    assert_equal 10, w

    p g
  end

  def test_height_at
    # w,h
    g = Grid.new(30, 20)

    # (col,row)
    h = g.height_at(0, 0)
    assert_equal 20, h

    g.split_col(0, 15)
    g.split_row(0, 10)

    h = g.height_at(0, 0)
    assert_equal 20, h

    g.set(1, 1, "X")
    
    h = g.height_at(0, 0)
    assert_equal 20, h

    h = g.height_at(0, 1)
    assert_equal 10, h

    p g
  end

  def test_free_at
    g = Grid.new(4,6)	

    assert_equal true, g.free_at(0, 0)

    g.split_row(0, 3)
    assert_equal true, g.free_at(0, 0)
    assert_equal true, g.free_at(0, 1)

    g.set(0, 1, "T")
    assert_equal false, g.free_at(0, 1)
  end
  
  def test_walk_across
    g = Grid.new(20, 20)
    g.split_col(0 ,10)

    count = 0
    g.walk_across(0, 0) { count += 1 }
    assert_equal 2, count
    
    g.set(1,0, "A")
    g.set(0,0, "a")

    p g

    count = 0
    g.walk_across(0, 0) { count += 1 }
    assert_equal 0, count
  end

  def test_walk_down
    g = Grid.new(60, 80)

    g.split_row(0, 40)
    g.split_row(1, 20)
    g.split_col(0, 30)

    count = 0
    g.walk_down(0, 0) do |cell| 
      p cell
      count += 1 
    end
    assert_equal 3, count

    g.set(0,1,"A")
    p "test_walk_down"
    p g

    count = 0
    g.walk_down(0, 0) { count += 1 }
    assert_equal 1, count

  end
end
